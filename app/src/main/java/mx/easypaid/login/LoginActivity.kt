package mx.easypaid.login

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_login.*
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val text_usuario = findViewById<TextView>(R.id.text_usuario)
        val text_contraseña = findViewById<TextView>(R.id.text_contrasena)

        //Obtenemos el componente gráfico del layout. El id del botón es btn_entrar
        val boton = findViewById<Button>(R.id.btn_entrar)
        val tache = findViewById<TextView>(R.id.X_textView)
        val tache2 = findViewById<TextView>(R.id.X2_textView)

        //Asignamos un listener para el evento de OnClick al botón
        // Mandamos a llamar el Evento Onclick de un botón

        boton.setOnClickListener {

            if(text_usuario.text.length<5){
                if(text_usuario.text.length==0) Toast.makeText(this.applicationContext, "Ingresa un correo electrónico", Toast.LENGTH_SHORT).show()
                    else if(text_usuario.text.length<5) Toast.makeText(this.applicationContext, "Correo electrónico incompleto", Toast.LENGTH_SHORT).show()
                        tache.visibility=View.VISIBLE
            }

            if(text_contraseña.text.length<8){
                if(text_contraseña.text.length==0) Toast.makeText(this.applicationContext, "Ingresa una contraseña", Toast.LENGTH_SHORT).show()
                    else if(text_contraseña.text.length<8) Toast.makeText(this.applicationContext, "Contraseña incompleta", Toast.LENGTH_SHORT).show()
                        tache2.visibility=View.VISIBLE
            }

            if(text_usuario.text.length>=5 && text_contraseña.text.length>=8){
                tache.visibility=View.INVISIBLE
                tache2.visibility=View.INVISIBLE

                var correo: String = text_usuario.text.toString()
                var contrasena: String = text_contraseña.text.toString()

                val TAG = "EasyPaid"
                //Localhost
                val basePath = "http://10.0.2.2:5000/api/"
                val controlador = "Login"

                // Instantiate the RequestQueue
                val requestQueue = Volley.newRequestQueue(this.applicationContext);

                //Parámetros de envío para la petición de Login
                val params =   HashMap<String, String>()
                params.put("email", correo)
                params.put("password", contrasena)

                val parameters = JSONObject(params)

                //Prepare the Request
                val jsonObjReq = object : JsonObjectRequest(
                        Request.Method.POST, //GET
                        basePath + controlador, //URL
                        parameters, //Parameters
                        Response.Listener<JSONObject> { response ->
                            Log.d(TAG, "/GET request OK! Response: $response")
                            Toast.makeText(this.applicationContext,"GET request OK", Toast.LENGTH_LONG).show()

                            val intent = Intent(this, MainActivity::class.java)
                            startActivity(intent)

                            //Preferences
                            val sharedPreferences = getSharedPreferences("InfoUsuario", Context.MODE_PRIVATE)

                            val editor = sharedPreferences.edit()
                            editor.putString("Usuario", text_usuario.text.toString())
                            editor.putString("Contraseña", text_contrasena.text.toString())
                            editor.apply()

                            Toast.makeText( this, "Datos guardados", Toast.LENGTH_LONG).show()
                        },
                        Response.ErrorListener { error ->
                            Log.d(TAG, "/GET request fail! Error: ${error.message}")
                            Toast.makeText(this.applicationContext,"Error en:  ${error.networkResponse.statusCode} : ${error.message}", Toast.LENGTH_LONG).show()
                        }) {
                            @Throws(AuthFailureError::class)
                                override fun getHeaders(): Map<String, String> {
                                    val headers = HashMap<String, String>()
                                        headers.put("Content-Type", "application/json")
                                            return headers
                                }
                        }
                jsonObjReq.tag = TAG
                requestQueue?.add(jsonObjReq)
            }
        }
    }
}
